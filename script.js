//A zónák értékeinek tárolására bevezetett class
class Zone {

    constructor(id) {

        this._id = id;
        this._isMine = 0; 
        this._minesAround = 0;
        this._isVisible = 0;
        this._checkedForMine = 0;

    }

}

//Az oldal betöltésekor lejátszódó animáció és a játék indítás;
function loadPage() {

    setTimeout(function() {createNewLine("Welcome to bDos");}, 0);
    setTimeout(function() {createNewLine("Version 0.19.02");}, 250);
    setTimeout(function() {createNewLine("");}, 251);
    setTimeout(function() {writer("load game \"Minesweeper\"")}, 500);
    setTimeout(function() {createNewLine("Game found");}, 2600);
    setTimeout(function() {createNewLine("Loading, please wait");}, 2650);
    setTimeout(function() {start();}, 4000);
}

//Egy új divet készít egy belső divvel amiben a szöveg/ szövegek találhatóak, illetve leteker az oldal legaljára
function createNewLine(innerText, id = "") {

	//külső div
    let outterDiv = document.createElement("DIV");
    outterDiv.className = "innerContainer";

	//belső div
    let innerDiv = document.createElement("DIV");
    innerDiv.className = "conDiv";
    innerDiv.innerHTML = innerText;
    innerDiv.id = id;

    outterDiv.appendChild(innerDiv);

	document.getElementById("container").appendChild(outterDiv);
	//Leteker az oldal aljára
    window.scrollTo(0, document.getElementById("container").scrollHeight);
}

//Egy írás animációt keltő funkció, 2 belső divvel és egy külsővel
function writer(innerText) {

	//Külső div
    let outterDiv = document.createElement("DIV");
    outterDiv.className = "innerContainer";

	//Belső div 1, csak a ">" jelért felel
    let innerDiv1 = document.createElement("DIV");
    innerDiv1.className = "conDiv";
    innerDiv1.innerHTML = ">";

	//Belső div 2
    let innerDiv2 = document.createElement("DIV");
    innerDiv2.className = "conDiv";

    outterDiv.appendChild(innerDiv1);
    outterDiv.appendChild(innerDiv2);

    document.getElementById("container").appendChild(outterDiv);

	let counter = 0;
	//Az írás hatás keltése
    let write = setInterval(() => {
        
        innerDiv2.innerHTML += innerText[counter];
        window.scrollTo(0, document.getElementById("container").scrollHeight);
        counter++;
        if (counter === innerText.length) {
            clearInterval(write);
        }

    }, 75);

}

//A játék "indítás" funkciója, ezzel lehet játékmódot választani, illetve a fölösleges onclickeket törli
function start() {
	if (document.getElementById("container").lastElementChild.children[0].children[0] !== undefined) {
		document.getElementById("container").lastElementChild.children[0].children[0].onclick = "";
	}
	createNewLine("There are 2 game modes. Click on \"<span onclick=\'game\(9\)\'>e<\/span>\" to play easy mode \(9x9 area with 10 mines\), or click on \"<span onclick=\"game\(16\)\">h<\/span>\" to play hard mode \(16x16 area with 40 mines\)");
}

//A játék fő divje, ez tartalmaz minden fontosabb funkciót amire szükség van a játék során
function game(size) {

    start();
    let time = 0; //Az eltelt idő
	let zones = []; //A zóna objektumok tárolására szolgáló tömb
	//Mérettől függően készíti el a pályát, és véletlenszerű bombákat készít, illetve a bombák számának jelőléséért felelős div elkészítése
    if (size===9) {
        createGameArea(9, zones);
        createMines(zones, size, 10);
        createNewLine("Checked for mine: 0 / 10", "checkedMines");
    } else if (size==16) {
        createGameArea(16, zones);
        createMines(zones, size, 40);
        createNewLine("Checked for mine: 0 / 40", "checkedMines");
    }
    else {
        createNewLine("Error! Size modified. Reloading page!");
        setTimeout(function() {location.reload()}, 2000);
	}
	//Az időt számláló div elkészítése
	createNewLine("Time elapsed: 0h 0m 0s", "timer");
	//Az időt számláló "funkció"
	let timer = setInterval(function() {
        time++;
        document.getElementById("timer").innerHTML = "Time elapsed: " + Math.floor(time/3600) + 'h ' + Math.floor((time/60)%60) + 'm ' + Math.floor((time/1)%60) + 's';
    }, 1000);
	checkMinesAroundForAll(zones, size);
	//Ellenőrzi hogy a játékos nyert e
	let win = setInterval(() => {
		let counter = 0;
		//A látható zónák száma alapján dönti el hogy nyertünk e
		for (let i = 0; i < size*size; i++) {

			if (zones[i]._isVisible !== 0) {
				counter++;
			}

		}
		//Ha 9x9-es területen nyerünk
		if (size===9&&counter===71) {

			for (let i = 0; i < size*size; i++) {

				if (zones[i]._isMine === 1) {
					//Elkészíti a zászlót mindenhova
					let newFlag = document.createElement("i");
					newFlag.className = "fas fa-flag";
					newFlag.style.width = "4vh";
					newFlag.style.width = "4vh";
					newFlag.style.color = "black";
					document.getElementById("gameArea").children[Number(zones[i]._id)].style.background = "white";
					if (document.getElementById("gameArea").children[Number(zones[i]._id)].children[0] === undefined) {
						document.getElementById("gameArea").children[Number(zones[i]._id)].appendChild(newFlag);
				}
			}
		}
			//Leállítja az időszámláló és a gyözelem ellenőrző "funkciót"
			clearInterval(win);
			clearInterval(timer);
			gameAreaDestruction(size);
			createNewLine("Congratulation! You find all the mines and save you life. Do you want to play again? Click on \"<span onclick=\'start\(\)\'>R<\/span>\" if yes");
		}
		//Ha 16x16-os területen nyerünk
		else if (size===16&&counter===216) {

			for (let i = 0; i < size*size; i++) {

				if (zones[i]._isMine === 1) {
					let newFlag = document.createElement("i");
					newFlag.className = "fas fa-flag";
					newFlag.style.width = "4vh";
					newFlag.style.width = "4vh";
					newFlag.style.color = "black";
					document.getElementById("gameArea").children[Number(zones[i]._id)].style.background = "white";
					if (document.getElementById("gameArea").children[Number(zones[i]._id)].children[0] !== undefined) {
						document.getElementById("gameArea").children[Number(zones[i]._id)].appendChild(newFlag);
					}
				}
			}
			clearInterval(win);
			clearInterval(timer);
			gameAreaDestruction(size);
			createNewLine("Congratulation! You find all the mines and save you life. Do you want to play again? Click on \"<span onclick=\'start\(\)\'>R<\/span>\" if yes");
		}
	}, 250);
	makeItClickable(zones, size, timer, win);
}

//A játékteret tartalmazó div és a benne levő mező divek elkészítése
function createGameArea(size, zones) {

	//Ha már van "gameArea" id-val rendelkező div akkor törli az id-t
    if (document.getElementById("gameArea")!==null) {
        document.getElementById("gameArea").id = "";
    }

	//Az új játéktér
    let newGameArea = document.createElement("DIV");
    newGameArea.style.width = size*5 + "vh";
    newGameArea.style.height = size*5 + "vh";
    newGameArea.id = "gameArea";

    for (let i = 0; i < size*size; i++) {

		//A játék mezői
        let newRect = document.createElement('div');
        newRect.style.margin = "0.25vh";
        newRect.style.border = "0.25vh solid black";
        newRect.style.width = "4vh";
        newRect.style.height = "4vh";
        newRect.style.float = "left";
        newRect.style.background = "#a6a6a6";
        newRect.style.textAlign = "center";
        newRect.style.fontSize = "3vh";
        newRect.style.lineHeight = "4.5vh";
        zones.push(new Zone(i));

        newGameArea.appendChild(newRect);

    }

    document.getElementById("container").appendChild(newGameArea);
    window.scrollTo(0, document.getElementById("container").scrollHeight);
    return 0;
}

//A játéktér mezőinek bal és jobbklikkre történő eseményeinek létrehozása
function makeItClickable(zones, size, interval, interval2 = undefined) {
    for (let i = 0; i < size*size; i++) {
        document.getElementById("gameArea").children[i].onclick = function() {

			//Bal klikk
            onLeftClick(this, i, zones, size, interval, interval2);

        };
        document.getElementById("gameArea").children[i].oncontextmenu = function() {
			
			//Jobb klikk
            onRightClick(this, i, zones, size);

        }
    }
}

//Ha olyan mezőre kattintunk ami körül nincs akna, akkor a körülötte levő területeket felfedi, rekurzív
function nearbyZeroMineZones(zones, size, id) {
	
	//A mező x kordinátája
	let idX = id%size;
	zones[id]._isVisible = 1;
	document.getElementById("gameArea").children[id].style.background = 'white';
	//Balra ellenőriz
    if ((id-1>=0)&&((id-1)%size<idX)) {
		
		//Ha a mellette levő zóna körül sincs akna
		if (zones[id-1]._minesAround === 0 && zones[id-1]._isVisible === 0) {
			
			zones[id-1]._isVisible = 1;
			document.getElementById("gameArea").children[id-1].background = 'white';
			nearbyZeroMineZones(zones, size, id-1);
			
		}
		
		//Ha a mellette levő zóna körül már található akna, ennek mennyiségétől függően változik a szín
		else if (zones[id-1]._minesAround !== 0){
			
			zones[id-1]._isVisible = 1;
			document.getElementById("gameArea").children[id-1].style.background = 'white';
			let color;
			switch(zones[id-1]._minesAround) {
				case 1: 
					color = 'blue';
					break;
				case 2:
					color = 'green';
					break;
				default:
					color = 'red';
					break;
			}
			document.getElementById("gameArea").children[id-1].style.color = color;
			document.getElementById("gameArea").children[id-1].innerHTML = zones[id-1]._minesAround;
		}
    }
	
	//Jobbra ellenőriz
    if ((id+1<size*size)&&(idX < (id+1)%size)) {
        
		if (zones[id+1]._minesAround === 0 && zones[id+1]._isVisible === 0) {
			
			zones[id+1]._isVisible = 1;
			document.getElementById("gameArea").children[id+1].background = 'white';
			nearbyZeroMineZones(zones, size, id+1);
			
		}
		
		else if (zones[id+1]._minesAround !== 0){
			
			zones[id+1]._isVisible = 1;
			document.getElementById("gameArea").children[id+1].style.background = 'white';
			let color;
			switch(zones[id+1]._minesAround) {
				case 1: 
					color = 'blue';
					break;
				case 2:
					color = 'green';
					break;
				default:
					color = 'red';
					break;
			}
			document.getElementById("gameArea").children[id+1].style.color = color;
			document.getElementById("gameArea").children[id+1].innerHTML = zones[id+1]._minesAround;
		}
    }
	
	//Felfele ellenőriz
    if ((id-size>=0)) {
        
		if (zones[id-size]._minesAround === 0 && zones[id-size]._isVisible === 0) {
			
			zones[id-size]._isVisible = 1;
			document.getElementById("gameArea").children[id-size].style.background = 'white';
			nearbyZeroMineZones(zones, size, id-size);
			
		}
		
		else if (zones[id-size]._minesAround !== 0){
			
			zones[id-size]._isVisible = 1;
			document.getElementById("gameArea").children[id-size].style.background = 'white';
			let color;
			switch(zones[id-size]._minesAround) {
				case 1: 
					color = 'blue';
					break;
				case 2:
					color = 'green';
					break;
				default:
					color = 'red';
					break;
			}
			document.getElementById("gameArea").children[id-size].style.color = color;
			document.getElementById("gameArea").children[id-size].innerHTML = zones[id-size]._minesAround;
		}
    }
	
	//Lefele ellenőriz
    if ((id+size<size*size)) {
        
		if (zones[id+size]._minesAround === 0 && zones[id+size]._isVisible === 0) {
			
			zones[id+size]._isVisible = 1;
			document.getElementById("gameArea").children[id+size].style.background = 'white';
			nearbyZeroMineZones(zones, size, id+size);
			
		}
		
		else if (zones[id+size]._minesAround !== 0){
			
			zones[id+size]._isVisible = 1;
			document.getElementById("gameArea").children[id+size].style.background = 'white';
			let color;
			switch(zones[id+size]._minesAround) {
				case 1: 
					color = 'blue';
					break;
				case 2:
					color = 'green';
					break;
				default:
					color = 'red';
					break;
			}
			document.getElementById("gameArea").children[id+size].style.color = color;
			document.getElementById("gameArea").children[id+size].innerHTML = zones[id+size]._minesAround;
		}
    }
	
	//Balra lefele ellenőriz
    if ((id+(size-1)<size*size)&&((id+(size-1))%size<idX)) {
        
		if (zones[id+(size-1)]._minesAround === 0 && zones[id+(size-1)]._isVisible === 0) {
			
			zones[id+(size-1)]._isVisible = 1;
			document.getElementById("gameArea").children[id+(size-1)].style.background = 'white';
			nearbyZeroMineZones(zones, size, id+(size-1));
			
		}
		
		else if (zones[id+(size-1)]._minesAround !== 0){
			
			zones[id+(size-1)]._isVisible = 1;
			document.getElementById("gameArea").children[id+(size-1)].style.background = 'white';
			let color;
			switch(zones[id+(size-1)]._minesAround) {
				case 1: 
					color = 'blue';
					break;
				case 2:
					color = 'green';
					break;
				default:
					color = 'red';
					break;
			}
			document.getElementById("gameArea").children[id+(size-1)].style.color = color;
			document.getElementById("gameArea").children[id+(size-1)].innerHTML = zones[id+(size-1)]._minesAround;
		}
		
    }
	
	//Balra felfele ellenőriz
    if ((id-(size+1)>=0)&&((id-(size+1))%size<idX)) {
		
		if (zones[id-(size+1)]._minesAround === 0 && zones[id-(size+1)]._isVisible === 0) {
			
			zones[id-(size+1)]._isVisible = 1;
			document.getElementById("gameArea").children[id-(size+1)].style.background = 'white';
			nearbyZeroMineZones(zones, size, id-(size+1));
			
		}
		
		else if (zones[id-(size+1)]._minesAround !== 0){
			
			zones[id-(size+1)]._isVisible = 1;
			document.getElementById("gameArea").children[id-(size+1)].style.background = 'white';
			let color;
			switch(zones[id-(size+1)]._minesAround) {
				case 1: 
					color = 'blue';
					break;
				case 2:
					color = 'green';
					break;
				default:
					color = 'red';
					break;
			}
			document.getElementById("gameArea").children[id-(size+1)].style.color = color;
			document.getElementById("gameArea").children[id-(size+1)].innerHTML = zones[id-(size+1)]._minesAround;
		}
    }
	
	//Jobb lefele ellenőriz
    if ((id+(size+1)<size*size)&&((id+(size+1))%size>idX)) {
		
		if (zones[id+(size+1)]._minesAround === 0 && zones[id+(size+1)]._isVisible === 0) {
			
			zones[id+(size+1)]._isVisible = 1;
			document.getElementById("gameArea").children[id+(size+1)].style.background = 'white';
			nearbyZeroMineZones(zones, size, id+(size+1));
			
		}
		
		else if (zones[id+(size+1)]._minesAround !== 0){
			
			zones[id+(size+1)]._isVisible = 1;
			document.getElementById("gameArea").children[id+(size+1)].style.background = 'white';
			let color;
			switch(zones[id+(size+1)]._minesAround) {
				case 1: 
					color = 'blue';
					break;
				case 2:
					color = 'green';
					break;
				default:
					color = 'red';
					break;
			}
			document.getElementById("gameArea").children[id+(size+1)].style.color = color;
			document.getElementById("gameArea").children[id+(size+1)].innerHTML = zones[id+(size+1)]._minesAround;
		}
    }
	
	//Jobb felfele ellenőriz
    if ((id-(size-1)>=0)&&((id-(size-1))%size>idX)) {
		
		if (zones[id-(size-1)]._minesAround === 0 && zones[id-(size-1)]._isVisible === 0) {
			
			zones[id-(size-1)]._isVisible = 1;
			document.getElementById("gameArea").children[id-(size-1)].style.background = 'white';
			nearbyZeroMineZones(zones, size, id-(size-1));
			
		}
		
		else if (zones[id-(size-1)]._minesAround !== 0){
			
			zones[id-(size-1)]._isVisible = 1;
			document.getElementById("gameArea").children[id-(size-1)].style.background = 'white';
			let color;
			switch(zones[id-(size-1)]._minesAround) {
				case 1: 
					color = 'blue';
					break;
				case 2:
					color = 'green';
					break;
				default:
					color = 'red';
					break;
			}
			document.getElementById("gameArea").children[id-(size-1)].style.color = color;
			document.getElementById("gameArea").children[id-(size-1)].innerHTML = zones[id-(size-1)]._minesAround;
		}
    }
	
}

//Bal kattintásra történő események
function onLeftClick(what, id, zones, size, interval, interval2) {
    if (zones[id]._isMine === 0 && zones[id]._isVisible === 0 && zones[id]._checkedForMine === 0) {

		//Ha olyan zónára kattintunk ami körül nincs akna
        if (zones[id]._minesAround === 0 && zones[id]._isMine === 0) { 
            nearbyZeroMineZones(zones, size, id)
		}
		
		//Ha olyan zónára kattintunk ami körül van akna
        else if (zones[id]._minesAround !== 0 && zones[id]._isMine === 0){
			zones[id]._isVisible = 1;
            what.style.background = "#FFFFFF";
            let color;
			switch(zones[id]._minesAround) {
				case 1: 
					color = 'blue';
					break;
				case 2:
					color = 'green';
					break;
				default:
					color = 'red';
					break;
            }
            what.style.color = color;
            what.innerHTML = zones[id]._minesAround;
		}
	}

	//Ha aknára kattintunk
	else if (zones[id]._isMine === 1 && zones[id]._checkedForMine === 0) {
		//Az időszámláló leállítása
		if (interval2!==undefined) {
			clearInterval(interval2);
		}
		//Jelöli az összes akna helyét, amire kattintottunk azt halványabban
		for (let i = 0; i < size*size; i++) {
			if (zones[i]._isMine === 1) {
				document.getElementById("gameArea").children[Number(zones[i]._id)].style.background = "red";
				if (document.getElementById("gameArea").children[Number(zones[i]._id)].children[0] !== undefined)
					document.getElementById("gameArea").children[Number(zones[i]._id)].children[0].remove();
			}
		}
		what.style.background = "#ff4242";
		//A "win funkció" leállítása
		clearInterval(interval);
		gameAreaDestruction(size);
		//A játék végi szöveg az újrakezdési lehetőséggel
		createNewLine("Better luck next time! Do you want to play again? Click on \"<span onclick=\'start\(\)\'>R<\/span>\" if yes")

	}
}

//Jobb kattintásra történő események
function onRightClick(element, id, zones, size) {

	//Aknának jelölt zónák számlálója
	let mineCheckedCounter = 0;
	for (let i = 0; i < zones.length; i++) {

		if (zones[i]._checkedForMine === 1) {
			mineCheckedCounter++;
		}
	}

	//A maximum aknák számának eldöntése
	let max = (size === 9) ? 10 : 40;

	//Ha aknának jelölünk egy zónát
    if (zones[id]._isVisible === 0 && zones[id]._checkedForMine === 0 && mineCheckedCounter < max) {

		zones[id]._checkedForMine = 1;
		mineCheckedCounter++;
		//Zászló készítés
		let newFlag = document.createElement("i");
		newFlag.className = "fas fa-flag";
		newFlag.style.width = "4vh";
		newFlag.style.width = "4vh";
		newFlag.style.color = "black";
		element.style.background = "white";
		element.appendChild(newFlag);
	}

	//Ha aknának jelölünk már aknának jelölt zónát, ezzel törölve a jelölést
	else if (zones[id]._checkedForMine === 1) {

		mineCheckedCounter--;
		zones[id]._checkedForMine = 0;
		element.children[0].remove();
		element.style.background = "#a6a6a6";

	}
	updateMineChecked(mineCheckedCounter, size);
}

//A jelölt aknánk vizuális megjelenítésének frissítő funkciója
function updateMineChecked(mineCheckedCounter, size) {

	if (size===9) {
		document.getElementById("checkedMines").innerHTML = "Checked for mine: " + mineCheckedCounter + " / 10";
	}
	else if (size===16) {
		document.getElementById("checkedMines").innerHTML = "Checked for mine: " + mineCheckedCounter + " / 40";
	}

}

//A felesleges onclick-ek és id-k törlése
function gameAreaDestruction(size) {
	for (let i = 0; i < size*size; i++) {
		document.getElementById("gameArea").children[i].onclick = "";
		document.getElementById("gameArea").children[i].oncontextmenu = "";
	}
	if (document.getElementById("timer") !== null) {
		document.getElementById("timer").id = "";
	}
	if (document.getElementById("checkedMines") !== null) {
		document.getElementById("checkedMines").id = "";
	}
}

//Véletlenszerű aknák elkészítése
function createMines(zones, size, mines) {
    let b = [];
    for (let i = 0; i < size*size; i++) {
       b[i] = i;
    }
    for (let i = 0; i < mines; i++) {
        let a = Math.floor(Math.random() * b.length);
        zones[b[a]]._isMine = 1;
        b.splice(a, 1);
  }
}

//Az összes mezőt körülvevő zónák ellenörzése
function checkMinesAroundForAll(zones, size) {

    for (let i = 0; i < zones.length; i++) {

        checkMinesAround(i, size, zones);

    }

}

//Egy adott mező ellenőrzése, hogy mennyi akna van körülötte
function checkMinesAround(id, size, area) {

    let counter = 0;
    let idX = id%size;
	
	//Balra ellenőriz
    if ((id-1>=0)&&((id-1)%size<idX)&&(area[id-1]._isMine === 1)) {
        counter++;
    }
	
	//Jobbra ellenőriz
    if ((id+1<size*size)&&(idX < (id+1)%size)&&(area[id+1]._isMine === 1)) {
        counter++;
    }
	
	//Felfele ellenőriz
    if ((id-size>=0)&&(area[id-size]._isMine === 1)) {
        counter++;
    }
	
	//Lefele ellenőriz
    if ((id+size<size*size)&&(area[id+size]._isMine === 1)) {
        counter++;
    }
	
	//Balra lefele ellenőriz
    if ((id+(size-1)<size*size)&&((id+(size-1))%size<idX)&&(area[id+(size-1)]._isMine === 1)) {
        counter++;
    }
	
	//Balra felfele ellenőriz
    if ((id-(size+1)>=0)&&((id-(size+1))%size<idX)&&(area[id-(size+1)]._isMine === 1)) {
        counter++;
    }
	
	//Jobb lefele ellenőriz
    if ((id+(size+1)<size*size)&&((id+(size+1))%size>idX)&&(area[id+(size+1)]._isMine === 1)) {
        counter++;
    }
	
	//Jobb felfele ellenőriz
    if ((id-(size-1)>=0)&&((id-(size-1))%size>idX)&&(area[id-(size-1)]._isMine === 1)) {
        counter++;
    }
	
    area[id]._minesAround = counter;
}